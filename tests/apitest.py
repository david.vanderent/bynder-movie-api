import unittest
import requests

class apitest(unittest.TestCase):   
    def test_getTopRatedMovies(self):
        '''200 succesful'''
        global movie_id
        global apikey
        apikey = "a08e5ecc271ab8ef3757b3117c7a327f"
        
        url = "https://api.themoviedb.org/3/movie/top_rated?" + "api_key=" + apikey + "&language=en-US&page=1"
        response = requests.request("GET", url)
        self.assertEqual(response.status_code, 200)
        json = response.json()
        movielist = json['results']
        movie_id = movielist[10]['id']

    def test_unAuthGetTopRatedMovies(self):
        '''401 unauthorized'''
        
        apikey = "invalid"
        url = "https://api.themoviedb.org/3/movie/top_rated?" + "api_key=" + apikey + "&language=en-US&page=1"
        response = requests.request("GET", url)
        self.assertEqual(response.status_code, 401)

    def test_notFoundGetTopRatedMovies(self):
        '''404 not found'''
        
        url = "https://api.themoviedb.org/3/movies/top_rated?" + "api_key=" + apikey + "&language=en-US&page=1"
        response = requests.request("GET", url)
        self.assertEqual(response.status_code, 404)


    def test_rateMovie(self):
        '''201 success'''
        global guest_session
        global movie_id        
        
        url = "https://api.themoviedb.org/3/authentication/guest_session/new" + "?api_key=" + apikey
        response = requests.request("GET", url)
        json = response.json()
        guest_session = json['guest_session_id']
        self.assertTrue(len(guest_session)>0)
        
        url = "https://api.themoviedb.org/3/movie/" + str(movie_id) + "/rating" + "?api_key=" + apikey + "&guest_session_id=" + guest_session
        headers = {'Content-Type': "application/json;charset=utf-8", 'Cache-Control': "no-cache"}
        response = requests.request("POST", url, json={"value": "7.5"})
        self.assertEqual(response.status_code, 201)

    def test_unAuthRateMovie(self):
        '''401 unauth'''
        apikey = "invalid"
                          
        url = "https://api.themoviedb.org/3/movie/" + str(movie_id) + "/rating" + "?api_key=" + apikey + "&guest_session_id=" + guest_session
        headers = {'Content-Type': "application/json;charset=utf-8", 'Cache-Control': "no-cache"}
        response = requests.request("POST", url, json={"value": "7.5"})
        self.assertEqual(response.status_code, 401)

    @unittest.skip("guest session issue")
    def test_notFoundRateMovie(self):
        '''404 not found'''
        
        url = "https://api.themoviedb.org/3/movie/" + str(movie_id) + "/rating" + "?api_key=" + apikey + "&guest_session_id=" + guest_session        
        headers = {'Content-Type': "application/json;charset=utf-8", 'Cache-Control': "no-cache"}
        response = requests.request("POST", url, json={"value": "7.5"})
        print(response.text)
        self.assertEqual(response.status_code, 404)



if __name__ == '__main__':
    unittest.main()