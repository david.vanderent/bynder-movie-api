FROM python:3
RUN pip install xmlrunner
RUN pip install requests
WORKDIR /usr/src/app
COPY . .
CMD ["runsuite.py"]
ENTRYPOINT ["python3"]