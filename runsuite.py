import unittest
import xmlrunner
from tests import apitest

suite = unittest.TestSuite([
    unittest.defaultTestLoader.loadTestsFromTestCase(apitest.apitest)
    ])

runner = xmlrunner.XMLTestRunner(output='test-reports')
runner.run(suite)       
