# bynder-movie-api

## Overview

This repo contains tests written for Bynder.

## Build container

First you need to build the docker container with this command

```bash
docker build -t movie-api .
```

## Run api tests

To execute the tests, enter the following commands:

```bash
docker run movie-api
```

## Test report
While running the tests, you see the result in your command window.
Inside the docker container an xml test report is created. Unfortunately, I dont know how to get it out, after the test has run...So, it is created there, but I dont know how to get it out.
To see the report, you would need to run the tests without the container on a machine with python;
copy the files to that machine and run:

```bash
python runsuite.py
```

Now an xml report is available in \test-report

